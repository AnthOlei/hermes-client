### IMPORTANT

IMPORTANT: I will not be making updates that have to do with features while
the wit.ai hackathon is live. I will only be making code changes, and will not 
be pushing them to the live webserver. These changes will include:

1. JS -> TS
2. Algorithm Optimizations
3. Testing