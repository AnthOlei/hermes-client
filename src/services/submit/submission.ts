import axios from 'axios'
import { generateServerUrl } from '../helpers'

async function makeSubmission(
  email: string,
  file: Blob,
  commands: string,
  endtime: number
): Promise<void> {
  const form = new FormData()
  form.append('file', file)
  form.append('email', email)
  form.append('commands', commands)
  form.append('endtime', endtime.toString())
  await axios.post(generateServerUrl('submit/'), form, {
    headers: { 'Content-Type': 'multipart/form-data' },
  })
}

export default makeSubmission
