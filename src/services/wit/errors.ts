enum WitError {
    NO_SPEECH_HEARD = 'No speech heard.',
    COULDNT_TRANSCRIBE = 'Couldn\'t decipher voice command.',
    NO_BODY_TEXT = 'No body text was specified for the caption.'
}

export default WitError