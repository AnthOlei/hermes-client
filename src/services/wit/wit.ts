import WitErrors from './errors'
import { Errors } from '../../constants/errors'
import { generateServerUrl } from '../helpers'
import backgroundColor from '../../helpers/BackgroundColor'
import axios from 'axios'
import 'path'
import IntentName from '../../data_classes/commands/CommandIntent'
import CaptionCommand from '../../data_classes/commands/CaptionCommand'
import MusicType from '../../data_classes/MusicTypes'
import DeleteCommand from '../../data_classes/commands/DeleteCommand'
import MusicCommand from '../../data_classes/commands/MusicCommand'
import MuteCommand from '../../data_classes/commands/MuteCommand'
import SpeedCommand from '../../data_classes/commands/SpeedCommand'
import Caption from '../../data_classes/context/Caption'
import { Command } from '../../data_classes/commands/Command'
import { defaultCaption } from '../../data_classes/default_caption'

interface TimeRange {
  range: [number, number]
}

interface Intent {
  name: IntentName
}

interface WitResponse {
  text: string
  intents: Intent[]
  entities: {
    'wit$message_body:message_body'?: {
      body: string
    }[]
    'color:color'?: {
      body: string
    }[]
    'music_theme:music_theme'?: {
      value: MusicType
    }[]
    'wit$datetime:datetime': {
      from: {
        value: string
      }
      to: {
        value: string
      }
    }[]
    'wit$number:number': {
      value: number
    }[]
  }
  traits: {
    'hermes_time-direction'?: {
      value: string
    }[]
  }
}

async function makeWitRequest(mp3: Blob): Promise<WitResponse> {
  try {
    const resp = await axios.post(generateServerUrl('wit/'), mp3, {
      headers: {
        'Content-Type': 'blob',
      },
    })
    return resp.data
  } catch (e) {
    throw Error(Errors.NO_CONNECTION)
  }
}

function parseWitRequest(
  response: WitResponse,
  time: number,
  totalTime: number | undefined
): Command {
  if (response.text) {
    console.info('Trancription: ', response.text)
  } else {
    throw Error(WitErrors.NO_SPEECH_HEARD)
  }

  if (response.intents.length < 1) {
    throw Error(WitErrors.COULDNT_TRANSCRIBE)
  }

  //determine intent and add any flavor that is necessary
  const intent: IntentName = response.intents[0].name

  let timeRange: TimeRange
  if (!totalTime) {
    timeRange = {
      range: [time, time],
    }
  } else {
    timeRange = determineTimeRange(time, response, totalTime)
  }

  return generateCommand(intent, timeRange, response)
}

function enrichMusic(response: WitResponse): MusicType {
  let musicType: MusicType
  if (response.entities['music_theme:music_theme']) {
    musicType = response.entities['music_theme:music_theme'][0].value
  } else {
    musicType = MusicType.background
  }

  return musicType
}

function enrichCaption(response: WitResponse): Caption {
  if (!response.entities['wit$message_body:message_body']) {
    throw WitErrors.NO_BODY_TEXT
  }

  const text = response.entities['wit$message_body:message_body'][0].body

  //determine color
  let color: string
  if (!response.entities['color:color']) {
    color = 'white'
  } else {
    color = response.entities['color:color'][0].body
  }
  const captionBackgroundColor = backgroundColor(color)

  return {
    ...defaultCaption,
    backgroundColor: captionBackgroundColor,
    color: color,
    text: text,
  }
}

function determineTimeRange(
  time: number,
  response: WitResponse,
  totalTime: number
): TimeRange {
  const base = {
    from: time,
    to: time,
  }

  // figure out what time to put it on
  if (response.entities['wit$datetime:datetime']) {
    const dateTimeEntity = response.entities['wit$datetime:datetime'][0]
    const diff =
      Date.parse(dateTimeEntity.to.value) -
      Date.parse(dateTimeEntity.from.value)

    if (getTimeDirection(response) === 'forward') {
      base.to += diff / 1000
    } else {
      //default to backward if no trait found
      base.from -= diff / 1000
    }
  } else if (response.entities['wit$number:number']) {
    const seconds = response.entities['wit$number:number'][0].value
    //assume that if we are talking about a floating number, "next 10", we are talking about seconds.

    if (getTimeDirection(response) === 'backward') {
      base.from -= seconds
    } else {
      base.to += seconds
    }
  } else {
    base.to += 10
  }

  if (base.from < 0) {
    base.from = 0
  }

  if (totalTime && base.to > totalTime) {
    base.to = totalTime
  }

  return {
    range: [base.from, base.to],
  }
}

function generateCommand(
  commandName: IntentName,
  timeRange: TimeRange,
  response: WitResponse
): Command {
  switch (commandName) {
    case IntentName.caption:
      return new CaptionCommand(
        timeRange.range[0],
        timeRange.range[1],
        enrichCaption(response)
      )
    case IntentName.cut:
      return new DeleteCommand(timeRange.range[0], timeRange.range[1])
    case IntentName.music:
      return new MusicCommand(
        timeRange.range[0],
        timeRange.range[1],
        enrichMusic(response)
      )
    case IntentName.mute:
      return new MuteCommand(timeRange.range[0], timeRange.range[1])
    case IntentName.timelapse:
      return new SpeedCommand(timeRange.range[0], timeRange.range[1], 2.0)
  }
  throw Error('Intent not acconted for')
}

//parse helpers
function getTimeDirection(response: WitResponse): string {
  if (response.traits['hermes_time-direction']) {
    return response.traits['hermes_time-direction'][0].value
  }
  return 'backward'
}

export { makeWitRequest, parseWitRequest }
