function generateServerUrl(path: string): string {
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    return process.env.REACT_APP_DEV_URL + path
  } else {
    return process.env.REACT_APP_PROD_URL + path
  }
}

export { generateServerUrl }
