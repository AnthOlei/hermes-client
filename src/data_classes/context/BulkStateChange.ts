// used for when Speech context passes data to MediaContext,

import Caption from './Caption'
import Music from './Music'
import { Command } from '../commands/Command'
import CaptionCommand from '../commands/CaptionCommand'
import MusicCommand from '../commands/MusicCommand'
import MuteCommand from '../commands/MuteCommand'
import SpeedCommand from '../commands/SpeedCommand'
import DeleteCommand from '../commands/DeleteCommand'
import IntentName from '../commands/CommandIntent'

// utility class that is checked to see what to display on screen
class BulkStateChange {
  timelapse: boolean
  mute: boolean
  caption: Caption | undefined
  music: Music | undefined

  constructor() {
    this.timelapse = false
    this.mute = false
    this.caption = undefined
    this.music = undefined
  }

  //always returns true, or throw error (for compile time switch exhaustion)
  public visitor(command: Command): boolean {
    switch (command.intent) {
      case IntentName.caption:
        this.caption = (command as CaptionCommand).caption
        return true
      case IntentName.music:
        this.music = (command as MusicCommand).musicType
        return true
      case IntentName.mute:
        this.mute = true
        return true
      case IntentName.timelapse:
        this.timelapse = true
        return true
      case IntentName.cut:
        throw Error('Should not have passed deleteCommand into visitor')
      case IntentName.none:
        return false
    }
  }
}

export default BulkStateChange
