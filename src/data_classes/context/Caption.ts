export default interface Caption {
  color: string
  text: string
  backgroundColor: string
  backgroundOpacity: string
  size: number
}
