import MusicType from '../MusicTypes'

export default interface Music {
  from: number
  musicType: MusicType
}
