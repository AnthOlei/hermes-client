interface Toast {
  hash: number
  title: string
  message: string
}

export default Toast
