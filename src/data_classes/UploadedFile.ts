export default interface UploadedFile {
    url: string,
    file: File
  }