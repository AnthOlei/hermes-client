import { Command } from './Command'
import IntentName from './CommandIntent'

export default class DeleteCommand extends Command {
  constructor(from: number, to: number) {
    super(from, to, IntentName.cut, '#ff3300')
  }
}
