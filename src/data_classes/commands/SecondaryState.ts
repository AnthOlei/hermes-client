enum SecondaryState {
  loading,
  error,
  none,
  noTimestamps,
}

export default SecondaryState
