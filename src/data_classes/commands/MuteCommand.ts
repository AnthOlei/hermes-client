import { Command } from './Command'
import IntentName from './CommandIntent'

export default class MuteCommand extends Command {
  constructor(from: number, to: number) {
    super(from, to, IntentName.mute, '#ffff00')
  }
}
