import { Command } from './Command'
import IntentName from './CommandIntent'

export default class SpeedCommand extends Command {
  public speed: number

  constructor(from: number, to: number, speed: number) {
    super(from, to, IntentName.timelapse, '#ffff00')
    this.speed = speed
  }
}
