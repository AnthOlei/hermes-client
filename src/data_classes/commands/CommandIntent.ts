enum IntentName {
  cut = 'Cut',
  caption = 'Caption',
  music = 'Music',
  mute = 'Mute',
  timelapse = 'Timelapse',
  none = 'None',
}

export default IntentName
