import MusicType from '../MusicTypes'
import { Command } from './Command'
import Music from '../context/Music'
import IntentName from './CommandIntent'

export default class MusicCommand extends Command {
  musicType: Music

  constructor(from: number, to: number, musicType: MusicType) {
    super(from, to, IntentName.music, '#cc00cc')
    this.musicType = {
      musicType: musicType,
      from: from,
    }
  }
}
