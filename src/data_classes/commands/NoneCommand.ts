import { Command } from './Command'
import IntentName from './CommandIntent'
import SecondaryState from './SecondaryState'

export default class NoneCommand extends Command {
  constructor(
    from: number,
    to: number,
    secondaryState: SecondaryState,
    selected: boolean
  ) {
    super(from, to, IntentName.none, '#ffffff')
    this.secondaryState = secondaryState
    this.selected = selected
  }
}
