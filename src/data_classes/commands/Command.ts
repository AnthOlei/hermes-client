import SecondaryState from './SecondaryState'
import IntentName from './CommandIntent'

export interface StrippedCommand extends Omit<Command,
  | 'hash' | 'intent' | 'color' | 'secondaryState' | 'selected' | 'stripCommand'> { }

export abstract class Command {
  public from: number
  public to: number
  public hash: number
  public intent: IntentName
  public color: string
  public secondaryState = SecondaryState.none
  public selected = false

  constructor(from: number, to: number, intent: IntentName, color: string) {
    this.from = from
    this.to = to
    this.color = color

    this.intent = intent
    this.hash = Date.now() //date.now is used as a hash due to gaurenteed uniqueness
  }

  stripCommand = (): StrippedCommand => {
    const { hash, intent, color, secondaryState, selected, stripCommand, ...rest } = this
    return rest
  }
}
