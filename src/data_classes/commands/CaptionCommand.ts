import { Command } from './Command'
import Caption from '../context/Caption'
import IntentName from './CommandIntent'
import { defaultCaption } from '../default_caption'

export default class CaptionCommand extends Command {
  public caption: Caption

  constructor(from: number, to: number, caption?: Caption) {
    super(from, to, IntentName.caption, '#0066cc')
    this.caption = caption ?? defaultCaption
  }
}
