enum MusicType {
  calm,
  background,
  sad,
  epic,
  energetic,
}

export default MusicType
