import Caption from './context/Caption'

export const defaultCaption: Caption = {
    backgroundColor: '#000000',
    backgroundOpacity: 'ff',
    color: '#ffffff',
    size: 24,
    text: ''
}
