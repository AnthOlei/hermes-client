/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from 'react'
import IToastContext from './toasts/IToastContext'
import { ToastContext } from './toasts/ToastContext'

import IMediaContext from './media/IMediaContext'
import { MediaContext } from './media/MediaContext'

import ISpeechContext from './speech/ISpeechContext'
import { SpeechContext } from './speech/SpeechContext'

export interface AddToastContext {
  toastContext: IToastContext
}

export function withToastContext<T extends AddToastContext = AddToastContext>(
  WrappedComponent: React.ComponentType<T>
) {
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component'

  const ComponentWithToastContext = (props: Omit<T, keyof AddToastContext>) => {
    return (
      <ToastContext.Consumer>
        {(context) => (
          <WrappedComponent {...(props as T)} toastContext={context} />
        )}
      </ToastContext.Consumer>
    )
  }

  ComponentWithToastContext.displayName = `withToastContext(${displayName})`

  return ComponentWithToastContext
}

export interface AddMediaContext {
  mediaContext: IMediaContext
}

export function withMediaContext<T extends AddMediaContext = AddMediaContext>(
  WrappedComponent: React.ComponentType<T>
) {
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component'

  const ComponentWithMediaContext = (props: Omit<T, keyof AddMediaContext>) => {
    return (
      <MediaContext.Consumer>
        {(context) => (
          <WrappedComponent {...(props as T)} mediaContext={context} />
        )}
      </MediaContext.Consumer>
    )
  }

  ComponentWithMediaContext.displayName = `withMediaContext(${displayName})`

  return ComponentWithMediaContext
}

export interface AddSpeechContext {
  speechContext: ISpeechContext
}

export function withSpeechContext<
  T extends AddSpeechContext = AddSpeechContext
>(WrappedComponent: React.ComponentType<T>) {
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component'

  const ComponentWithSpeechContext = (
    props: Omit<T, keyof AddSpeechContext>
  ) => {
    return (
      <SpeechContext.Consumer>
        {(context) => (
          <WrappedComponent {...(props as T)} speechContext={context} />
        )}
      </SpeechContext.Consumer>
    )
  }

  ComponentWithSpeechContext.displayName = `withSpeechContext(${displayName})`

  return ComponentWithSpeechContext
}
