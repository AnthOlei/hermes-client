import Toast from '../../data_classes/Toast'

interface IToastContext {
  toasts: Toast[]
  addToast: (body: string, title: string, time?: number) => void
  removeToast: (hash: number) => void
}

export default IToastContext
