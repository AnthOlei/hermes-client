import React from 'react'
import IToastContext from './IToastContext'

const baseToastContext: IToastContext = {
  toasts: [],
  addToast: () => {
    return
  },
  removeToast: () => {
    return
  },
}

const ToastContext = React.createContext(baseToastContext)
export { ToastContext, baseToastContext }
