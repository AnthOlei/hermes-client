import React from 'react'
import { ToastContext, baseToastContext } from './ToastContext'
import BaseToastState from './IToastContext'
import IToastContext from './IToastContext'

class ToastState extends React.Component<unknown, BaseToastState> {
  public state: BaseToastState = {
    ...baseToastContext,
  }

  render(): JSX.Element {
    return (
      <ToastContext.Provider
        value={{
          toasts: this.state.toasts,
          addToast: this.addToast.bind(this),
          removeToast: this.removeToast.bind(this),
        }}
      >
        {this.props.children}
      </ToastContext.Provider>
    )
  }

  addToast(message: string, title: string, length?: number): void {
    const { toasts } = this.state

    const hash = Date.now()
    this.setState({
      toasts: [
        ...toasts,
        {
          message: message,
          title: title,
          hash: hash,
        },
      ],
    })

    if (length !== -1) {
      setTimeout(() => {
        this.removeToast(hash)
      }, length ?? 4000)
    }
  }

  removeToast(hash: number): void {
    this.setState({ toasts: this.state.toasts.filter((v) => v.hash !== hash) })
  }
}

export default ToastState
