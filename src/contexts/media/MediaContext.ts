import React from 'react'
import IBaseMediaState from './IMediaContext'

const baseMediaState: IBaseMediaState = {
  uploadMedia: () => {
    return undefined
  },
  skipTo: () => {
    return undefined
  },
  changeMuteState: () => {
    return undefined
  },
  changePlayState: () => {
    return undefined
  },
  updateCurrentTime: () => {
    return undefined
  },
  confirmSkip: () => {
    return undefined
  },
  adjustPreviewTime: () => {
    return undefined
  },
  bulkStateChange: () => {
    return undefined
  },
  mediaSrc: '',
  audioOnly: undefined,
  currentMediaTime: 0,
  mediaIsPlaying: false,
  totalMediaTime: undefined,
  mediaIsMuted: false,
  mediaIsTimelapsed: false,
  mediaCaption: undefined,
  addedMusic: undefined,
  file: undefined,
}

const MediaContext = React.createContext(baseMediaState)
export { MediaContext, baseMediaState }
