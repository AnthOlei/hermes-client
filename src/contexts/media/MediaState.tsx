import React, { createRef } from 'react'
import BaseMediaState from './IMediaContext'
import { withToastContext, AddToastContext } from '../HOCS'
import { MediaContext, baseMediaState } from './MediaContext'
import BulkStateChange from '../../data_classes/context/BulkStateChange'
import UploadedFile from '../../data_classes/UploadedFile'

class MediaState extends React.Component<AddToastContext, BaseMediaState> {
  public state: BaseMediaState = {
    ...baseMediaState,
  }

  private internalVideoRef = createRef<HTMLVideoElement>()

  render(): JSX.Element {
    return (
      <MediaContext.Provider
        value={{
          ...this.state,
          uploadMedia: this.uploadMedia.bind(this),
          skipTo: this.skipTo.bind(this),
          changePlayState: this.changePlayState.bind(this),
          changeMuteState: this.changeMuteState.bind(this),
          updateCurrentTime: this.updateCurrentTime.bind(this),
          confirmSkip: this.confirmSkip.bind(this),
          adjustPreviewTime: this.adjustPreviewTime.bind(this),
          bulkStateChange: this.bulkStateChange.bind(this),
        }}
      >
        {this.props.children}
        <video
          src={this.state.mediaSrc}
          ref={this.internalVideoRef}
          hidden={true}
        />
      </MediaContext.Provider>
    )
  }

  updateCurrentTime = (currentTime: number) => {
    this.setState({
      currentMediaTime: currentTime,
    })
  }

  uploadMedia = (uploadedFile: UploadedFile) => {
    this.setState(
      {
        mediaSrc: uploadedFile.url,
        audioOnly: false,
        file: uploadedFile.file,
      },
      this.getVideoDuration
    )
  }

  getVideoDuration(): void {
    const { toastContext } = this.props

    const onLoaded = () => {
      const internalVideoRef = this.internalVideoRef.current
      if (!internalVideoRef) {
        return
      }

      if (internalVideoRef.duration > 60 * 60) {
        toastContext.addToast(
          'Sorry, your file is too large. Please keep files up to one hour in length.',
          'Upload File Error'
        )
        setTimeout(() => this.resetMedia(), 400) //timeout due to not-instant uploading
      }

      this.setState({ totalMediaTime: internalVideoRef.duration })
      internalVideoRef.removeEventListener('loadedmetadata', onLoaded)
    }

    if (this.internalVideoRef.current) {
      this.internalVideoRef.current.addEventListener('loadedmetadata', onLoaded)
    }
  }

  changePlayState(playState: boolean | undefined): void {
    const { mediaIsPlaying, mediaSrc, currentMediaTime } = this.state

    if (!mediaSrc) return
    const newStateIsPlaying = playState ?? !mediaIsPlaying

    if (newStateIsPlaying) {
      this.setState({
        mediaIsPlaying: newStateIsPlaying,
      })
    } else {
      this.setState({
        previewTime: currentMediaTime,
        mediaIsPlaying: newStateIsPlaying,
      })
    }
  }

  changeMuteState(muteState: boolean): void {
    const { mediaIsMuted } = this.state

    this.setState({
      mediaIsMuted: muteState ?? !mediaIsMuted,
    })
  }

  bulkStateChange(stateChanges: BulkStateChange): void {
    const {
      mediaIsMuted,
      mediaIsTimelapsed,
      mediaCaption,
      addedMusic,
    } = this.state
    if (
      mediaIsTimelapsed !== stateChanges.timelapse ||
      mediaIsMuted !== stateChanges.mute ||
      !!mediaCaption !== !!stateChanges.caption ||
      !!addedMusic !== !!stateChanges.music
    ) {
      this.setState({
        mediaIsTimelapsed: stateChanges.timelapse,
        mediaIsMuted: stateChanges.mute,
        mediaCaption: stateChanges.caption,
        addedMusic: stateChanges.music,
      })
    }
  }

  resetMedia = () => {
    this.setState({
      mediaSrc: undefined,
      audioOnly: false,
      currentMediaTime: 0,
      mediaIsPlaying: false,
      totalMediaTime: 0,
      mediaIsMuted: false,
      skipToTime: undefined,
      file: undefined,
    })
  }

  skipTo = (timeStamp: number) => {
    const { mediaSrc } = this.state
    if (!mediaSrc) return

    this.setState({ skipToTime: timeStamp, currentMediaTime: timeStamp })
  }

  confirmSkip = () => {
    this.setState({ skipToTime: undefined })
  }

  adjustPreviewTime = (time: number) => {
    this.setState({ previewTime: time })
  }
}

export default withToastContext(MediaState)
