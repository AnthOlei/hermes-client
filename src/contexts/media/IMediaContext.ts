import Music from '../../data_classes/context/Music'
import Caption from '../../data_classes/context/Caption'
import BulkStateChange from '../../data_classes/context/BulkStateChange'
import UploadedFile from '../../data_classes/UploadedFile'

interface IMediaState {
  readonly uploadMedia: (uploadedFile: UploadedFile) => void
  readonly skipTo: (time: number) => void
  readonly changeMuteState: (changeTo: boolean) => void
  readonly updateCurrentTime: (time: number) => void
  readonly confirmSkip: () => void
  readonly adjustPreviewTime: (time: number) => void
  readonly bulkStateChange: (allChangedStateCommands: BulkStateChange) => void
  readonly changePlayState: (newPlayState: boolean | undefined) => void
  readonly mediaSrc: string | undefined
  readonly audioOnly?: boolean
  readonly currentMediaTime?: number
  readonly mediaIsPlaying: boolean
  readonly totalMediaTime?: number
  readonly mediaIsMuted: boolean
  readonly mediaIsTimelapsed: boolean
  readonly mediaCaption?: Caption
  readonly addedMusic?: Music
  readonly skipToTime?: number
  readonly previewTime?: number
  readonly file?: Blob
}

export default IMediaState
