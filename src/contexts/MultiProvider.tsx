import React from 'react'
import ToastState from './toasts/ToastState'
import MediaState from './media/MediaState'
import SpeechState from './speech/SpeechState'

interface IMultiProvider {
  children: JSX.Element[]
}

const MultiProvider = (props: IMultiProvider) => {
  //speechState reads from both media and toast, and therefore needs to be centermost.
  //mediastate reads from toaststate, and therefore must be after toaststate.
  return (
    <ToastState>
      <MediaState>
        <SpeechState>{props.children}</SpeechState>
      </MediaState>
    </ToastState>
  )
}

export default MultiProvider
