import React from 'react'
import { SpeechContext, baseSpeechContext } from './SpeechContext'
import ISpeechContext from './ISpeechContext'
import {
  withMediaContext,
  withToastContext,
  AddToastContext,
  AddMediaContext,
} from '../HOCS'
import { makeWitRequest, parseWitRequest } from '../../services/wit/wit'
import WitError from '../../services/wit/errors'
import { Errors } from '../../constants/errors'
import { secondsToHms } from '../../helpers/dateTimeUtilities'
import MicRecorder from 'mic-recorder-to-mp3'
import DeleteCommand from '../../data_classes/commands/DeleteCommand'
import BulkStateChange from '../../data_classes/context/BulkStateChange'
import { Command, StrippedCommand } from '../../data_classes/commands/Command'
import SecondaryState from '../../data_classes/commands/SecondaryState'
import IntentName from '../../data_classes/commands/CommandIntent'
import { SpeechContextMode } from '../../constants/enums'
import { TIME_FOR_TOAST_ON_SPEECH_ERROR } from '../../constants/constants'

interface ISpeechProps extends AddToastContext, AddMediaContext { }

class SpeechState extends React.Component<ISpeechProps, ISpeechContext> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private recorder: any

  public state = {
    ...baseSpeechContext,
  }

  componentDidMount() {
    this.recorder = new MicRecorder({
      bitRate: 128,
    })
  }

  componentDidUpdate(_: ISpeechProps, prevState: ISpeechContext) {
    const { listening, listenForKeyboardEvents } = this.state
    this.editMediaState()
    if (!listening && prevState.listening) {
      this.onStopRecording(this.recorder.stop().getMp3())
    } else if (listening && !prevState.listening && listenForKeyboardEvents) {
      this.recorder.start()
    }
  }

  render() {
    return (
      <SpeechContext.Provider
        value={{
          ...this.state,
          changeListeningState: this.changeListeningState.bind(this),
          addCommandSection: this.addCommandSection.bind(this),
          removeCommandSection: this.removeCommandSection.bind(this),
          amendCommandSection: this.amendCommandSection.bind(this),
          getCommandsJsonified: this.getCommandsJsonified.bind(this),
          setListenForKeyboardEvents: this.setListenForKeyboardEvents.bind(this),
          flipSpeechContextMode: this.flipSpeechContextMode.bind(this),
        }}
      >
        {this.props.children}
      </SpeechContext.Provider>
    )
  }

  setListenForKeyboardEvents = (newVal: boolean) => {
    this.setState({ listenForKeyboardEvents: newVal })
  }

  flipSpeechContextMode = () => {
    if (this.state.speechContextMode === SpeechContextMode.Speech) {
      this.setState({ speechContextMode: SpeechContextMode.Text })

      //if any commands are in "error" mode, convert them to "select" mode
      this.state.commandSections.forEach((commandSection) => {
        if (commandSection.secondaryState === SecondaryState.error && commandSection.intent === IntentName.none) {
          commandSection.secondaryState = SecondaryState.none
          this.amendCommandSection(commandSection, false)
        }
      })
    } else {
      this.setState({ speechContextMode: SpeechContextMode.Speech })

      //if any commands are in "select" mode, convert them to "error" mode
      this.state.commandSections.forEach((commandSection) => {
        if (commandSection.secondaryState === SecondaryState.none && commandSection.intent === IntentName.none) {
          commandSection.secondaryState = SecondaryState.error
          this.amendCommandSection(commandSection, false)
        }
      })
    }
  }

  onStopRecording = async (mp3Promise: Promise<[Blob, never]>) => {
    const { hashCurrentlyListening } = this.state
    const { mediaContext } = this.props
    this.setState({ hashCurrentlyListening: undefined })
    const blob: Blob = (await mp3Promise)[1]

    if (hashCurrentlyListening) {
      this.makeHashTiedRequest(hashCurrentlyListening, blob)
    } else {
      this.makeFloatingRequest(mediaContext.currentMediaTime as number, blob)
    }
  }

  changeListeningState = (listeningState: boolean | undefined) => {
    const { listening } = this.state

    this.setState({
      listening: listeningState ?? !listening,
    })
    return true
  }

  addCommandSection = (
    commandSection: Command,
    setCurrentlyListening: boolean
  ) => {
    const { commandSections } = this.state

    if (!commandSection.hash) {
      commandSection.hash = Date.now()
    }

    this.setState({
      commandSections: [...commandSections, commandSection],
    })

    if (setCurrentlyListening) {
      this.setState({
        hashCurrentlyListening: commandSection.hash,
        listening: true,
      })
    }

    this.editMediaState() // immediately reflect changes in UI
  }

  removeCommandSection = (hash: number) => {
    const { commandSections } = this.state

    const withRemoved = commandSections.filter((cs) => cs.hash !== hash)
    this.setState({
      commandSections: withRemoved
    })

    this.editMediaState() // immediately reflect changes in UI
  }

  getCommandSection(hash: number): Command | undefined {
    const { commandSections } = this.state

    return commandSections.find(
      (commandSection) => commandSection.hash === hash
    )
  }

  amendCommandSection(
    changedCommandSection: Command,
    setCurrentlyListening: boolean
  ): void {
    const { commandSections } = this.state
    const foundCommandSectionIndex = commandSections.findIndex(
      (commandSection) => commandSection.hash === changedCommandSection.hash
    )
    if (foundCommandSectionIndex === -1) {
      console.error('couldn\'t find hash ', changedCommandSection.hash)
      return
    }

    const newCommandSection: Command = {
      ...commandSections.splice(foundCommandSectionIndex, 1)[0],
      ...changedCommandSection,
    }

    if (setCurrentlyListening) {
      this.setState({
        commandSections: [...commandSections, newCommandSection],
        hashCurrentlyListening: changedCommandSection.hash,
        listening: true,
      })
    } else {
      this.setState({
        commandSections: [...commandSections, newCommandSection],
      })
    }

    this.editMediaState() // immediately reflect changes in UI
  }

  editMediaState(): void {
    const { mediaContext } = this.props
    const { commandSections } = this.state

    const stateChanges: BulkStateChange = new BulkStateChange()

    const currentTime: number = mediaContext.currentMediaTime ?? 0

    for (let i = 0; i < commandSections.length; i++) {
      if (
        commandSections[i].from < currentTime &&
        commandSections[i].to > currentTime
      ) {
        const commandSection = commandSections[i]
        if (commandSection.intent === IntentName.cut) {
          mediaContext.skipTo(commandSection.to)
          return
        } else if (commandSection.from > currentTime) {
          break
        } else {
          stateChanges.visitor(commandSection)
        }
      }
    }

    mediaContext.bulkStateChange(stateChanges)
  }

  async makeHashTiedRequest(hash: number, blob: Blob): Promise<void> {
    const currentCommandSection = this.getCommandSection(hash)
    if (!currentCommandSection) {
      return
    }

    this.amendCommandSection(
      { ...currentCommandSection, secondaryState: SecondaryState.loading },
      false
    )
    try {
      const command = parseWitRequest(
        await makeWitRequest(blob),
        currentCommandSection.from,
        undefined
      )

      this.amendCommandSection(
        {
          ...command,
          from: currentCommandSection.from,
          to: currentCommandSection.to,
          hash: currentCommandSection.hash,
        },
        false
      )
    } catch (e) {
      this.handleError(e, this.getCommandSection(hash)?.from, hash)
    }
  }

  makeFloatingRequest = async (currentMediaTime: number, mp3: Blob) => {
    const { floatingRequests } = this.state
    const { totalMediaTime } = this.props.mediaContext

    this.setState({ floatingRequests: floatingRequests + 1 })
    try {
      const newCommand = parseWitRequest(
        await makeWitRequest(mp3),
        currentMediaTime,
        totalMediaTime
      )

      this.addCommandSection(newCommand, false)
    } catch (e) {
      this.handleError(e, currentMediaTime, undefined)
    }
    this.setState({ floatingRequests: this.state.floatingRequests - 1 })
  }

  handleError = (
    e: Error,
    time: number | undefined,
    hash: number | undefined
  ) => {
    const { toastContext } = this.props

    let beginningMessage: string

    let commandSection: Command | undefined
    if (hash) {
      commandSection = this.getCommandSection(hash)
    }

    if (commandSection) {
      this.amendCommandSection(
        { ...commandSection, secondaryState: SecondaryState.error },
        false
      )
      beginningMessage = `Error while creating command near ${secondsToHms(
        time
      )}`
    } else {
      beginningMessage = `Error while creating floating command near ${secondsToHms(
        time
      )}`
    }
    if (e.message === WitError.NO_BODY_TEXT) {
      toastContext.addToast(
        `${beginningMessage}: ${WitError.NO_BODY_TEXT}`,
        'Command Creating Error',
        TIME_FOR_TOAST_ON_SPEECH_ERROR
      )
    } else if (e.message === Errors.NO_CONNECTION) {
      toastContext.addToast(
        `${beginningMessage}: ${Errors.NO_CONNECTION}`,
        'Network Error',
        TIME_FOR_TOAST_ON_SPEECH_ERROR
      )
    } else if (e.message === WitError.COULDNT_TRANSCRIBE) {
      toastContext.addToast(
        `${beginningMessage}: ${WitError.COULDNT_TRANSCRIBE}`,
        'Command Creating Error',
        TIME_FOR_TOAST_ON_SPEECH_ERROR
      )
    } else if (e.message === WitError.NO_SPEECH_HEARD) {
      console.log('Transcription: NO SPEECH HEARD')
      toastContext.addToast(
        `${beginningMessage}: ${WitError.NO_SPEECH_HEARD}`,
        'Command Creating Error',
        TIME_FOR_TOAST_ON_SPEECH_ERROR
      )
    } else {
      console.error(e)
      toastContext.addToast(
        `${beginningMessage}: Undefined error.`,
        e.message ?? 'Undefined Error',
        TIME_FOR_TOAST_ON_SPEECH_ERROR
      )
    }
  }

  getCommandsJsonified = () => {
    const { commandSections } = this.state
    interface Jsonified {
      Cut: StrippedCommand[]
      Timelapse: StrippedCommand[]
      Mute: StrippedCommand[]
      Caption: StrippedCommand[]
      Music: StrippedCommand[]
      None: StrippedCommand[]
    }

    const jsonified: Jsonified = {
      Cut: [],
      Timelapse: [],
      Mute: [],
      Caption: [],
      Music: [],
      None: [],
    }

    //this forces some rad compile time behavior
    Object.values(IntentName).forEach((intent) => jsonified[intent])

    commandSections.forEach((commandSection) =>
      jsonified[commandSection.intent as keyof Jsonified].push(commandSection.stripCommand())
    )
    return JSON.stringify(jsonified)
  }
}

export default withToastContext(withMediaContext(SpeechState))
