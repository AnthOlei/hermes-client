import React from 'react'
import { SpeechContextMode } from '../../constants/enums'
import ISpeechContext from './ISpeechContext'

const baseSpeechContext: ISpeechContext = {
  listening: false,
  listenForKeyboardEvents: true,
  commandSections: [],
  hashCurrentlyListening: undefined,
  floatingRequests: 0,
  speechContextMode: SpeechContextMode.Text,
  changeListeningState: () => {
    return undefined
  },
  addCommandSection: () => {
    return undefined
  },
  removeCommandSection: () => {
    return undefined
  },
  amendCommandSection: () => {
    return undefined
  },
  getCommandsJsonified: () => {
    return ''
  },
  setListenForKeyboardEvents: () => {
    return {}
  },
  flipSpeechContextMode: () => {
    return undefined
  }
}

const SpeechContext = React.createContext(baseSpeechContext)
export { SpeechContext, baseSpeechContext }
