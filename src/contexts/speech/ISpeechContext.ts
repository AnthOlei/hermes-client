import { SpeechContextMode } from '../../constants/enums'
import { Command } from '../../data_classes/commands/Command'

interface ISpeechContext {
  readonly listening: boolean
  readonly listenForKeyboardEvents: boolean
  readonly commandSections: Command[]
  readonly hashCurrentlyListening: number | undefined
  readonly floatingRequests: number
  readonly speechContextMode: SpeechContextMode
  readonly changeListeningState: (to: boolean | undefined) => void
  readonly addCommandSection: (
    commandSection: Command,
    setCurrentlyListening: boolean
  ) => void
  readonly removeCommandSection: (hash: number) => void
  readonly amendCommandSection: (
    changedCommandSection: Command,
    setCurrentlyListening: boolean
  ) => void
  readonly getCommandsJsonified: () => string
  readonly setListenForKeyboardEvents: (canListenState: boolean) => void
  readonly flipSpeechContextMode: () => void
}

export default ISpeechContext
