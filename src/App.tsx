/* eslint-disable react/jsx-filename-extension */
import React from 'react'
import MediaTimeline from './components/main_sections/media_timeline/MediaTimeline'
import MediaTools from './components/main_sections/media_tools/MediaTools'
import 'bootstrap/dist/css/bootstrap.css'
import './styles/styles.css'
import './styles/layout.css'
import MultiProvider from './contexts/MultiProvider'
import MediaPlayer from './components/main_sections/media_player/MediaPlayer'
import Toaster from './components/toasts/Toaster'
import ExtraAudioPlayer from './components/helper_components/ExtraAudioPlayer'
import { Scale } from './components/main_sections/media_timeline/helper_components/ScaleProvider'

function App(): JSX.Element {
  return (
    <div className="App">
      <MultiProvider>
        <div className="Page">
          <MediaTools />
          <MediaPlayer />
          <Scale.Provider>
            <MediaTimeline />
          </Scale.Provider>
        </div>
        <ExtraAudioPlayer />
        <Toaster />
      </MultiProvider>
    </div>
  )
}

export default App
