import { } from './timelineConstants'

function adjustPixelsToSeconds(pixels: number, scale: number): number {
  return pixels / scale
}

function adjustSecondsToPixels(seconds: number, scale: number): number {
  return seconds * scale
}

export { adjustPixelsToSeconds, adjustSecondsToPixels }
