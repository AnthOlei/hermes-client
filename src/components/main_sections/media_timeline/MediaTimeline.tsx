import React, { useContext, useRef, useState } from 'react'
import TimeBar from './helper_components/TimeBar'
import { adjustPixelsToSeconds } from './helpers'
import { RAW_MOUSE_OFFSET } from './timelineConstants'
import { MediaContext } from '../../../contexts/media/MediaContext'
import { SpeechContext } from '../../../contexts/speech/SpeechContext'
import MediaDisplayBar from './helper_components/MediaDisplayBar'
import CurrentTimeIndicator from './helper_components/CurrentTimeIndicator'
import CommandSection from './helper_components/command_section/CommandSection'
import './media_timeline.css'
import SecondaryState from '../../../data_classes/commands/SecondaryState'
import NoneCommand from '../../../data_classes/commands/NoneCommand'
import { Command } from '../../../data_classes/commands/Command'
import { SpeechContextMode } from '../../../constants/enums'
import { Scale } from './helper_components/ScaleProvider'
import { useContainer } from 'unstated-next'

const MediaTimeline = () => {
  const [hoverPixels, setHoverPixels] = useState<number | undefined>(undefined)
  const [mouseIsIn, setMouseIsIn] = useState(false)

  const timelineScrollRef = useRef<HTMLDivElement | null>(null)
  const speechContext = useContext(SpeechContext)
  const mediaContext = useContext(MediaContext)
  const scale = useContainer(Scale)

  const onMouseEnterTrack = () => setMouseIsIn(true)
  const onMouseExitTrack = () => setMouseIsIn(false)

  const onHoverTrack = (x: number) => {
    mediaContext.adjustPreviewTime(
      adjustPixelsToSeconds(x, scale.scale)
    )

    setHoverPixels(adjustPixelsToScroll(x))
  }

  const onDragTrack = (clickDownBeginsAt: number) => {
    const addedCommandSection: Command = new NoneCommand(
      adjustPixelsToSeconds(adjustPixelsToScroll(clickDownBeginsAt), scale.scale),
      adjustPixelsToSeconds(
        (hoverPixels ?? mediaContext.currentMediaTime ?? clickDownBeginsAt) + 30, scale.scale
      ),
      SecondaryState.none,
      true
    )

    speechContext.addCommandSection(addedCommandSection, speechContext.speechContextMode === SpeechContextMode.Speech)
  }

  const onClickTrack = (mouseDownAt: number): void => {
    if (!mouseDownAt) return //sometimes, event will get called when accidently dragging off commandSection.
    mediaContext.skipTo(adjustPixelsToSeconds(adjustPixelsToScroll(mouseDownAt), scale.scale))
  }

  const adjustPixelsToScroll = (pixels: number): number => (timelineScrollRef.current?.scrollLeft ?? 0) + pixels - RAW_MOUSE_OFFSET



  return (
    <div className="video-timeline main-section video-timeline">
      <div className="timeline__time_timestamp">
        <MediaContext.Consumer>
          {(context) => (
            <TimeBar
              currentTime={
                context.mediaIsPlaying
                  ? context.currentMediaTime
                  : context.previewTime
              }
              totalTime={context.totalMediaTime}
            />
          )}
        </MediaContext.Consumer>
      </div>
      <div
        className="timeline__time_track m-2 p-4 mt-5"
        ref={timelineScrollRef}
      >
        <SpeechContext.Consumer>
          {(context) =>
            context.commandSections.map((commandSection) => (
              <CommandSection
                command={commandSection}
                key={commandSection.hash}
              />
            ))
          }
        </SpeechContext.Consumer>
        <MediaContext.Consumer>
          {(context) =>
            context.mediaSrc && (
              <CurrentTimeIndicator
                opaque={true}
                seconds={context.currentMediaTime}
              />
            )
          }
        </MediaContext.Consumer>
        {mouseIsIn && hoverPixels && (
          <CurrentTimeIndicator
            seconds={adjustPixelsToSeconds(hoverPixels, scale.scale)}
            opaque={false}
          />
        )}
        <MediaDisplayBar
          onClickTrack={onClickTrack}
          onHoverTrack={onHoverTrack}
          onDragTrack={onDragTrack}
          onMouseEnterTrack={onMouseEnterTrack}
          onMouseExitTrack={onMouseExitTrack}
        />
      </div>
    </div>
  )
}

export default MediaTimeline
