import React from 'react'
import { Form } from 'react-bootstrap'
import { SpeechContextMode } from '../../../../constants/enums'
import { withSpeechContext } from '../../../../contexts/HOCS'
import { SpeechContext } from '../../../../contexts/speech/SpeechContext'
import { secondsToHms } from '../../../../helpers/dateTimeUtilities'
import { Scale } from './ScaleProvider'


interface ITimeBarProps {
  currentTime: number | undefined
  totalTime: number | undefined
}

export function TimeBar(props: ITimeBarProps) {
  const { currentTime, totalTime } = props

  const scale = Scale.useContainer()

  return (
    <div className="timebar p-2 divided">
      <>
        <SpeechContext.Consumer>
          {(context) => (
            <Form.Check
              defaultChecked={context.speechContextMode === SpeechContextMode.Speech}
              type="switch"
              label={<Form.Label className={'text-secondary'}>
                {context.speechContextMode === SpeechContextMode.Speech ? 'Speech Enabled' : 'Speech Disabled'}
              </Form.Label>}
              id="speech-switch"
              onClick={context.flipSpeechContextMode}
            />
          )}
        </SpeechContext.Consumer>
        <div className="mx-auto d-table">
          <span>{secondsToHms(currentTime ?? 0)}&nbsp;</span>/{' '}
          {secondsToHms(totalTime ?? 0)}
        </div>
        <div className="mx-5 px-3 timebar__scale_slider">
          <Form.Control
            type="range"
            value={scale.scale * 2}
            onChange={(e) => scale.setScale(Math.round((parseInt(e.target.value) || 1) / 2))} />
        </div>
      </>
    </div>
  )
}

export default TimeBar
