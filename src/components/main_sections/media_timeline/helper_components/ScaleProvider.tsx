import { useState } from 'react'
import { createContainer } from 'unstated-next'

function useScale(initalScale: number | undefined) {
    const [scale, setScale] = useState(initalScale ?? 1)

    return { scale, setScale }
}

export const Scale = createContainer(useScale)