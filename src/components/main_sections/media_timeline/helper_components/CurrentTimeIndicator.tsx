import React from 'react'
import { Scale } from './ScaleProvider'

interface ICurrentTimeIndicatorProps {
  opaque: boolean
  seconds: number | undefined
}

const CurrentTimeIndicator = (props: ICurrentTimeIndicatorProps) => {
  const { opaque, seconds } = props
  const scale = Scale.useContainer().scale

  if (!seconds) {
    return <div />
  }

  return (
    <div
      className="timeline__current_time_indicator"
      style={{
        opacity: opaque ? 1.0 : 0.6,
        marginLeft: (scale * seconds ?? 0) + 'px',
      }}
    />
  )
}

export default CurrentTimeIndicator
