import React, { useContext, useEffect, useState } from 'react'
import { RAW_MOUSE_OFFSET } from '../../timelineConstants'
import { adjustPixelsToSeconds, adjustSecondsToPixels } from '../../helpers'
import BlinkingEllipses from '../../../../helper_components/BlinkingEllipses'
import {
  withMediaContext,
  AddMediaContext,
  AddSpeechContext,
  withSpeechContext,
} from '../../../../../contexts/HOCS'
import { Command } from '../../../../../data_classes/commands/Command'
import IntentName from '../../../../../data_classes/commands/CommandIntent'
import SecondaryState from '../../../../../data_classes/commands/SecondaryState'
import ResponsiveButton from '../../../../helper_components/ResponsiveButton'
import { CommandSectionDropdown } from './CommandSectionDropdown'
import CaptionCommand from '../../../../../data_classes/commands/CaptionCommand'
import DeleteCommand from '../../../../../data_classes/commands/DeleteCommand'
import { SettingsDropdown } from './settings_dropdown/SettingsDropdown'
import { CaptionMenu } from './settings_dropdown/CaptionMenu'
import { SpeechContext } from '../../../../../contexts/speech/SpeechContext'
import { Scale } from '../ScaleProvider'
import { MediaContext } from '../../../../../contexts/media/MediaContext'

enum ClickDownDirecton {
  left = 1,
  right,
}

interface ICommandSectionProps extends AddMediaContext, AddSpeechContext {
  command: Command
}

const CommandSection = (props: ICommandSectionProps) => {
  const { command } = props

  const scale = Scale.useContainer()
  const speechContext = useContext(SpeechContext)
  const mediaContext = useContext(MediaContext)

  const [direction, setDirection] = useState<ClickDownDirecton | undefined>(command.selected
    ? ClickDownDirecton.right
    : undefined)
  const [widthInPixels, setWidthInpixels] = useState(adjustSecondsToPixels(command.to - command.from, scale.scale))
  const [tempChangedWidthInPixels, setTempChangedWidthInPixels] = useState(0)
  const [tempChangedMarginInPixels, setTempChangedMarginInPixels] = useState(0)

  useEffect(() => {
    document.addEventListener('keydown', deleteDown, false)

    return () => {
      document.removeEventListener('keydown', deleteDown, false)
    }
  })

  useEffect(() => {
    const newWidth = adjustSecondsToPixels(command.to - command.from, scale.scale)
    if (widthInPixels !== newWidth) {
      setWidthInpixels(newWidth)
    }
  })

  const buildSettingsDropdown = (): JSX.Element | undefined => {
    switch (command.intent) {
      case IntentName.caption:
        return <CaptionMenu command={command as CaptionCommand} />
      default:
        return
    }
  }

  const buildHeader = (): JSX.Element => {
    switch (command.secondaryState) {
      case SecondaryState.error:
        return ResponsiveButton({
          className: 'btn btn-outline-danger btn-sm',
          width:
            adjustSecondsToPixels(command.to - command.from, scale.scale) +
            tempChangedWidthInPixels,
          onClick: requestSpeech,
          text: ['Redo', 'Retry Recording'],
        })
      case SecondaryState.loading:
        return <BlinkingEllipses prefix="Loading" />
    }

    if (speechContext.hashCurrentlyListening === command.hash) {
      return <BlinkingEllipses prefix="Listening" />
    }

    if (command.intent === IntentName.none) {
      return <CommandSectionDropdown onSelect={manuallySetType} command={command} />
    }

    return <p>{command.intent.toUpperCase()}</p>
  }

  const manuallySetType = (intentType: IntentName) => {
    let newCommand: Command
    switch (intentType) {
      case IntentName.caption:
        newCommand = new CaptionCommand(command.from, command.to)
        break
      default:
        newCommand = new DeleteCommand(command.from, command.to)
    }
    newCommand.hash = command.hash
    speechContext.amendCommandSection(newCommand, false)
  }

  const onMouseDown = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {

    const clickedOnPixel =
      e.clientX - RAW_MOUSE_OFFSET - adjustSecondsToPixels(command.from, scale.scale)
    if (clickedOnPixel < widthInPixels / 2) {
      setDirection(ClickDownDirecton.left)
    } else {
      setDirection(ClickDownDirecton.right)
    }
  }

  const onMouseUp = () => {
    if (tempChangedMarginInPixels !== 0 || tempChangedWidthInPixels !== 0) {
      speechContext.amendCommandSection(
        {
          ...command,
          from: command.from + adjustPixelsToSeconds(tempChangedMarginInPixels, scale.scale),
          to:
            command.to +
            adjustPixelsToSeconds(tempChangedWidthInPixels, scale.scale) +
            adjustPixelsToSeconds(tempChangedMarginInPixels, scale.scale),
        },
        false
      )

      if (speechContext.hashCurrentlyListening === command.hash) {
        speechContext.changeListeningState(false)
      }
      setDirection(undefined)
      setTempChangedMarginInPixels(0)
      setTempChangedWidthInPixels(0)
      setDirection(undefined)
    }
  }

  const onMouseMove = (e: React.MouseEvent<HTMLElement, MouseEvent>): void => {
    if (direction === ClickDownDirecton.left) {
      const newPixelMarginLeft = tempChangedMarginInPixels + e.movementX

      if (
        adjustPixelsToSeconds(newPixelMarginLeft, scale.scale) + command.from < 0 &&
        e.movementX < 0
      ) {
        return
      }
      setTempChangedMarginInPixels(newPixelMarginLeft)
    } else if (direction === ClickDownDirecton.right) {
      const newPixelWidth = tempChangedWidthInPixels + e.movementX

      if (
        adjustPixelsToSeconds(newPixelWidth, scale.scale) + command.to >
        (mediaContext.totalMediaTime as number) &&
        e.movementX > 0
      ) {
        return
      }

      setTempChangedWidthInPixels(newPixelWidth)
    }
  }

  const deleteDown = (e: KeyboardEvent) => {
    if (e.key === 'Backspace' && direction) {
      speechContext.removeCommandSection(command.hash)
    }

    if (command.hash === speechContext.hashCurrentlyListening) {
      speechContext.changeListeningState(false)
    }
  }

  const requestSpeech = (): void => {
    speechContext.amendCommandSection(
      { ...command, secondaryState: SecondaryState.none },
      true
    )
  }

  return (
    <section
      className={
        'timeline__command_section' + (direction ? '--selected' : '')
      }
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      onMouseLeave={onMouseUp}
      onMouseMove={onMouseMove}
      onDrag={(e) => e.preventDefault()}
      style={{
        marginLeft:
          tempChangedMarginInPixels + adjustSecondsToPixels(command.from, scale.scale),
        width: widthInPixels + tempChangedWidthInPixels + 'px',
      }}
    >
      <div className={'timeline__command_section__title'} onMouseDown={e => e.preventDefault()}>
        {buildHeader()}
      </div>
      <div
        className="timeline__command_section__frosted"
        style={{ backgroundColor: `${command.color}34` }}
      >
        <SpeechContext.Consumer>
          {(context) => buildSettingsDropdown() && <SettingsDropdown onClick={() => context.amendCommandSection(command, false)} >
            {buildSettingsDropdown()}
          </SettingsDropdown>
          }
        </SpeechContext.Consumer>
      </div>
    </section>
  )
}

export default withSpeechContext(withMediaContext(CommandSection))
