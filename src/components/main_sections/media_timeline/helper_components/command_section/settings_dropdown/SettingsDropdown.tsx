import React, { FunctionComponent } from 'react'
import { Popover, OverlayTrigger } from 'react-bootstrap'
import { GearFill } from 'react-bootstrap-icons'

interface ISettingsDropdown {
    onClick?: () => void
}


export const SettingsDropdown: FunctionComponent<ISettingsDropdown> = ({ children, onClick }): JSX.Element => {
    const stopProp = (event: React.MouseEvent) => event.stopPropagation()

    const overlayContents = <Popover id="popover-basic">
        <Popover.Content onClick={stopProp} onMouseDown={stopProp}>
            {children}
        </Popover.Content>
    </Popover>


    return <OverlayTrigger trigger="click" placement="right" overlay={overlayContents}>
        <GearFill className='timeline__command_section__gear' onClick={onClick} onMouseDown={stopProp} />
    </OverlayTrigger>
}