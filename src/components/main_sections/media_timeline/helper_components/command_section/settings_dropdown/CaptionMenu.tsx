import React, { useContext } from 'react'
import { FormControl, Dropdown, Form } from 'react-bootstrap'
import { SpeechContext } from '../../../../../../contexts/speech/SpeechContext'
import CaptionCommand from '../../../../../../data_classes/commands/CaptionCommand'
import { CompactPicker, AlphaPicker, RGBColor } from 'react-color'

interface ICaptionMenuProps {
    command: CaptionCommand
}

export const CaptionMenu = (props: ICaptionMenuProps): JSX.Element => {
    const { command } = props

    const speechContext = useContext(SpeechContext)

    const amendText = (str: string) => {
        command.caption.text = str
        speechContext.amendCommandSection(command, false)
    }

    const amendSize = (size: number) => {
        command.caption.size = ((size / 100) * 72) + 12
        speechContext.amendCommandSection(command, false)
    }

    const amendColor = (color: string) => {
        command.caption.color = color
        speechContext.amendCommandSection(command, false)
    }

    const amendBgColor = (color: string) => {
        command.caption.backgroundColor = color
        speechContext.amendCommandSection(command, false)
    }

    const amendOpacity = (opacity: number) => {
        let hex = (opacity * 255).toString(16)

        if (hex.includes('.')) {
            hex = hex.split('.')[0]
        }

        if (hex.length === 1) {
            hex = 0 + hex
        }

        command.caption.backgroundOpacity = hex
        console.log(command.caption)
        console.log(command.caption.color + command.caption.backgroundOpacity)
        speechContext.amendCommandSection(command, false)

        console.log(hex)
    }

    return <>
        <FormControl
            autoFocus
            onFocus={() => speechContext.setListenForKeyboardEvents(false)}
            onBlur={() => speechContext.setListenForKeyboardEvents(true)}
            className="my-2 w-auto"
            placeholder="Enter Caption Here..."
            onChange={(e) => amendText(e.target.value)}
        />
        <Dropdown.Divider />
        <Form.Label >Font:</Form.Label>
        <Form.Control type="range" className="mb-2" value={(((command.caption.size * 100) / 72) - 12).toString()} onChange={(e) => amendSize(parseInt(e.target.value))} />
        <CompactPicker className="w-auto mb-2" onChange={(color) => amendColor(color.hex)} color={command.caption.color} />
        <Dropdown.Divider />
        <Form.Label >Background:</Form.Label>
        <CompactPicker className="w-auto mb-2" onChange={(color) => amendBgColor(color.hex)} color={command.caption.backgroundColor} />
        <AlphaPicker className="w-auto mb-2"
            onChange={(value) => amendOpacity(value.rgb.a as number)}
            color={command.caption.backgroundColor + command.caption.backgroundOpacity} />
    </>
}