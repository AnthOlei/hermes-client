import React, { useContext } from 'react'
import { ButtonGroup, Dropdown, DropdownButton } from 'react-bootstrap'
import { SpeechContext } from '../../../../../contexts/speech/SpeechContext'
import { Command } from '../../../../../data_classes/commands/Command'
import IntentName from '../../../../../data_classes/commands/CommandIntent'

interface ICommandSectionDropdownProps {
    onSelect: (p0: IntentName) => void
    command: Command
}

export const CommandSectionDropdown = (props: ICommandSectionDropdownProps): JSX.Element => {
    const fn = (type: string) => {
        props.onSelect(IntentName[type as keyof typeof IntentName])
    }

    const speechContext = useContext(SpeechContext)

    return <DropdownButton
        as={ButtonGroup}
        onMouseDown={(event) => {
            event.stopPropagation()
            speechContext.amendCommandSection(props.command, false)
        }}
        id={'dropdown-button-type'}
        size="sm"
        variant="primary"
        title="Select Type"
        drop="down"
    >
        {Object.keys(IntentName).map((command) =>
            <Dropdown.Item
                onClick={() => fn(command)}
                eventKey={command}
                key={command}>
                {IntentName[command as keyof typeof IntentName]}
            </Dropdown.Item>)}
    </DropdownButton>
}