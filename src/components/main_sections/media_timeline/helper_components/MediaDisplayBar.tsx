import React, { useState } from 'react'
import { MediaContext } from '../../../../contexts/media/MediaContext'
import { Scale } from './ScaleProvider'
import { useContainer } from 'unstated-next'

const TIME_TO_BE_DRAG = 200

interface IMediaDisplayBarProps {
  onHoverTrack: (arg0: number) => void
  onDragTrack: (arg0: number) => void
  onClickTrack: (arg0: number) => void
  onMouseEnterTrack: () => void
  onMouseExitTrack: () => void
}

const MediaDisplayBar = (props: IMediaDisplayBarProps) => {
  const scale = useContainer(Scale)
  const { onHoverTrack, onDragTrack, onClickTrack, onMouseEnterTrack, onMouseExitTrack } = props
  const [dragging, setDragging] = useState(false)
  const [mouseIsIn, setMouseIsIn] = useState(false)
  const [mouseDown, setMouseDown] = useState<number | undefined>(undefined)

  let mouseUpInterval: NodeJS.Timeout | undefined

  const onMouseDown = (e: React.MouseEvent): void => {
    e.persist()
    setMouseDown(e.clientX)
    mouseUpInterval = (setTimeout(() => isDragging(e.clientX), TIME_TO_BE_DRAG))
  }

  const onMouseUp = (): void => {
    if (!dragging) {
      onClickTrack(mouseDown as number)
    }
    setMouseDown(undefined)
    setDragging(false)
    if (mouseUpInterval) clearInterval(mouseUpInterval)
  }

  const isDragging = (x: number): void => {
    onDragTrack(x)
    setDragging(true)
  }

  const onMouseEnter = (): void => {
    setMouseIsIn(true)
    onMouseEnterTrack()
  }

  const onMouseExit = (): void => {
    setMouseIsIn(false)
    onMouseExitTrack()
  }

  const onMouseMove = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
    if (mouseIsIn) {
      onHoverTrack(e.clientX)
    }
  }

  return (
    <MediaContext.Consumer>
      {(context) => (
        <div
          className="timeline__time_track__preview"
          style={{
            width:
              scale.scale * (context.totalMediaTime ?? 0) + 'px',
          }}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseExit}
          onMouseMove={onMouseMove}
          onMouseDown={onMouseDown}
          onMouseUp={onMouseUp}
        />
      )}
    </MediaContext.Consumer>
  )
}

export default MediaDisplayBar
