import React from 'react'
import './media_player.css'
import MusicSymbol from './helper_components/MusicSymbol'
import { withMediaContext, AddMediaContext, AddSpeechContext, withSpeechContext } from '../../../contexts/HOCS'
import { MediaContext } from '../../../contexts/media/MediaContext'
import IMediaState from '../../../contexts/media/IMediaContext'

interface IMediaPlayerState {
  muted: boolean
  timelapsed: boolean
}

interface IMediaPlayerProps extends AddMediaContext, AddSpeechContext { }

class MediaPlayer extends React.PureComponent<
  IMediaPlayerProps,
  IMediaPlayerState
  > {
  private videoPlayerRef = React.createRef<HTMLVideoElement>()
  private reportVideoStateInterval: ReturnType<typeof setInterval> | undefined

  public readonly state: Readonly<IMediaPlayerState> = {
    muted: false,
    timelapsed: false,
  }

  constructor(props: IMediaPlayerProps) {
    super(props)
    this.spaceDown = this.spaceDown.bind(this)
  }

  componentDidUpdate() {
    const { mediaIsPlaying, currentMediaTime } = this.props.mediaContext
    this.determineMediaState()
    this.determineMediaPreview()
    const currVideoPlayer = this.videoPlayerRef.current
    if (
      mediaIsPlaying &&
      currVideoPlayer &&
      (currVideoPlayer?.paused ?? false)
    ) {
      currVideoPlayer.currentTime = currentMediaTime ?? 0
      currVideoPlayer.play()
    } else if (!mediaIsPlaying && currVideoPlayer) {
      currVideoPlayer.pause()
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.spaceDown, false)
    this.reportVideoStateInterval = setInterval(
      this.reportVideoState.bind(this),
      200
    )
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.spaceDown, false)
    //TODO figure out how to clear the interval... ReturnType<typeof setInterval> doesnt work
  }

  render() {
    const { muted } = this.state
    return (
      <MediaContext.Consumer>
        {(context) => (
          <div className="media-player main-section p-3 border-left position-relative video-player">
            <div className="position-relative media-holder">
              {context.audioOnly && <MusicSymbol />}
              {context.mediaCaption && (
                <p
                  className="caption-text"
                  style={{
                    color: context.mediaCaption.color,
                    backgroundColor: context.mediaCaption.backgroundColor + context.mediaCaption.backgroundOpacity,
                    fontSize: context.mediaCaption.size + 'px'
                  }}
                >
                  {context.mediaCaption.text}
                </p>
              )}
              <video
                loop={false}
                src={context.mediaSrc}
                ref={this.videoPlayerRef}
                className="media-display"
                muted={muted}
              ></video>
            </div>
            <div className="mx-auto d-table pt-3">
              {this.renderControls(context)}
            </div>
          </div>
        )}
      </MediaContext.Consumer>
    )
  }

  renderControls = (context: IMediaState) => {
    return (
      <>
        <svg
          width="1.5em"
          height="1.5em"
          viewBox="0 0 16 16"
          className="bi bi-skip-start-fill mx-3"
          fill="#24292e"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => context.skipTo(0)}
        >
          <path
            fillRule="evenodd"
            d="M4.5 3.5A.5.5 0 0 0 4 4v8a.5.5 0 0 0 1 0V4a.5.5 0 0 0-.5-.5z"
          />
          <path d="M4.903 8.697l6.364 3.692c.54.313 1.232-.066 1.232-.697V4.308c0-.63-.692-1.01-1.232-.696L4.903 7.304a.802.802 0 0 0 0 1.393z" />
        </svg>
        <svg
          width="1.5em"
          height="1.5em"
          viewBox="0 0 16 16"
          className="bi bi-skip-backward-fill mx-1"
          fill="#24292e"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            if (context.currentMediaTime) {
              context.skipTo(Math.max(context.currentMediaTime - 15, 0))
            }
          }}
        >
          <path
            fillRule="evenodd"
            d="M.5 3.5A.5.5 0 0 0 0 4v8a.5.5 0 0 0 1 0V4a.5.5 0 0 0-.5-.5z"
          />
          <path d="M.904 8.697l6.363 3.692c.54.313 1.233-.066 1.233-.697V4.308c0-.63-.692-1.01-1.233-.696L.904 7.304a.802.802 0 0 0 0 1.393z" />
          <path d="M8.404 8.697l6.363 3.692c.54.313 1.233-.066 1.233-.697V4.308c0-.63-.693-1.01-1.233-.696L8.404 7.304a.802.802 0 0 0 0 1.393z" />
        </svg>
        {!context.mediaIsPlaying ? (
          <svg
            width="2.4em"
            height="2.4em"
            viewBox="0 0 16 16"
            className="bi bi-play-fill mx-1"
            fill="#24292e"
            xmlns="http://www.w3.org/2000/svg"
            onClick={() => context.changePlayState(true)}
          >
            <path d="M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z" />
          </svg>
        ) : (
            <svg
              width="2.4em"
              height="2.4em"
              viewBox="0 0 16 16"
              className="bi bi-pause-fill mx-1"
              fill="#24292e"
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => context.changePlayState(false)}
            >
              <path d="M5.5 3.5A1.5 1.5 0 0 1 7 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5zm5 0A1.5 1.5 0 0 1 12 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5z" />
            </svg>
          )}
        <svg
          width="1.5em"
          height="1.5em"
          viewBox="0 0 16 16"
          className="bi bi-skip-forward-fill mx-1 text-center"
          fill="#24292e"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            if (context.totalMediaTime && context.currentMediaTime) {
              context.skipTo(
                Math.min(context.totalMediaTime, context.currentMediaTime + 15)
              )
            }
          }}
        >
          <path
            fillRule="evenodd"
            d="M15.5 3.5a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5z"
          />
          <path d="M7.596 8.697l-6.363 3.692C.693 12.702 0 12.322 0 11.692V4.308c0-.63.693-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z" />
          <path d="M15.096 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.693-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z" />
        </svg>
        <svg
          width="1.5em"
          height="1.5em"
          viewBox="0 0 16 16"
          className="bi bi-skip-end-fill mx-3"
          fill="#24292e"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            if (context.totalMediaTime) {
              context.skipTo(context.totalMediaTime)
            }
          }}
        >
          <path
            fillRule="evenodd"
            d="M12 3.5a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5z"
          />
          <path d="M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z" />
        </svg>
      </>
    )
  }

  spaceDown = (e: KeyboardEvent) => {
    const { changePlayState } = this.props.mediaContext
    const { listenForKeyboardEvents } = this.props.speechContext
    if (e.key === ' ' && listenForKeyboardEvents) {
      changePlayState(undefined)
    }
  }

  reportVideoState = () => {
    const {
      updateCurrentTime,
      mediaSrc,
      mediaIsPlaying,
    } = this.props.mediaContext

    if (mediaSrc && mediaIsPlaying && this.videoPlayerRef.current) {
      updateCurrentTime(this.videoPlayerRef.current.currentTime)
    }
  }

  determineMediaState = () => {
    const {
      skipToTime,
      confirmSkip,
      mediaIsTimelapsed,
      mediaIsMuted,
    } = this.props.mediaContext
    const { muted, timelapsed } = this.state

    if (skipToTime && this.videoPlayerRef.current) {
      this.videoPlayerRef.current.currentTime = skipToTime
      confirmSkip()
      return
    }

    let changed = false
    if (mediaIsTimelapsed && !timelapsed && this.videoPlayerRef.current) {
      this.videoPlayerRef.current.playbackRate = 2.0
      changed = true
    } else if (
      !mediaIsTimelapsed &&
      timelapsed &&
      this.videoPlayerRef.current
    ) {
      this.videoPlayerRef.current.playbackRate = 1.0
      changed = true
    } else if (mediaIsMuted !== muted) {
      changed = true
    }

    if (changed) {
      this.setState({
        muted: mediaIsMuted,
        timelapsed: mediaIsTimelapsed,
      })
    }
  }

  determineMediaPreview = () => {
    const { previewTime, mediaIsPlaying } = this.props.mediaContext

    if (!mediaIsPlaying && previewTime && this.videoPlayerRef.current) {
      this.videoPlayerRef.current.currentTime = previewTime
    }
  }
}

export default withSpeechContext(withMediaContext(MediaPlayer))
