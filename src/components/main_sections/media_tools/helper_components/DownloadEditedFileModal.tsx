import React from 'react'
import DefaultModal from '../../../helper_components/DefaultModal'
import Form from 'react-bootstrap/Form'
import {
  withMediaContext,
  withSpeechContext,
  AddMediaContext,
  AddSpeechContext,
} from '../../../../contexts/HOCS'
import {
  secondsToHms,
  HmsToMinutes,
} from '../../../../helpers/dateTimeUtilities'
import makeSubmission from '../../../../services/submit/submission'
import Spinner from 'react-bootstrap/Spinner'

interface IDownloadEditedFileModalProps
  extends AddMediaContext,
  AddSpeechContext {
  show: boolean
  onClose: () => void
}

const DownloadEditedFileModal = (props: IDownloadEditedFileModalProps) => {
  const [emailInvalid, setEmailInvalid] = React.useState(false)
  const [needEmail, setNeedEmail] = React.useState(false)
  const [submitted, setSubmitted] = React.useState(false)
  const [loading, setLoading] = React.useState(false)
  const [error, setError] = React.useState('')
  const emailInput = React.createRef<HTMLInputElement>()

  const { show, speechContext, mediaContext, onClose } = { ...props }

  if (show && speechContext.listenForKeyboardEvents) {
    speechContext.setListenForKeyboardEvents(false)
  }

  const close = () => {
    speechContext.setListenForKeyboardEvents(true)
    setSubmitted(false)
    setLoading(false)
    setError('')
    onClose()
  }

  const send = async () => {
    const email: undefined | string = emailInput?.current?.value
    if (loading) {
      return
    }

    if (!email) {
      setNeedEmail(true)
      setEmailInvalid(false)
    } else if (!email.match(new RegExp(/[^@]+@[^.]+\..+/))) {
      setNeedEmail(false)
      setEmailInvalid(true)
    } else if (!mediaContext.file || !mediaContext.totalMediaTime) {
      setError('Sorry, we couldn\'t find your uploaded file.')
    } else {
      setNeedEmail(false)
      setEmailInvalid(false)
      setLoading(true)
      try {
        await makeSubmission(
          email,
          mediaContext.file,
          speechContext.getCommandsJsonified(),
          mediaContext.totalMediaTime
        )
        setSubmitted(true)
      } catch (e) {
        setLoading(false)
        setError(e.message)
      }
    }
  }

  let meat
  if (submitted) {
    meat = (
      <div>Submitted! We&apos;ll send you and email when we&apos;re done editing.</div>
    )
  } else if (loading) {
    meat = (
      <div className="d-flex justify-content-center">
        <Spinner animation="border" variant="primary" />
      </div>
    )
  } else {
    meat = (
      <>
        {error && (
          <div className="alert alert-danger" role="alert">
            {error}
          </div>
        )}
        <p>
          Your video will be rendered in the cloud on our servers - saving your
          computer the hard work of rendering video. After you download your
          video, It&apos;ll be completely removed from our servers. This process
          takes about 0.7 times your raw video length, so you can expect your
          video to be finshed finshed rendering in{' '}
          {HmsToMinutes(secondsToHms(0.7 * (mediaContext.totalMediaTime ?? 0)))}{' '}
          after it&apos;s your turn in the queue. When done rendering, we&apos;ll send you
          an email. We promise to not sell or give away your email.
          <br />
          NOTE: Please make sure to check your spam!
        </p>
        <hr />
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="name@example.com"
          ref={emailInput}
          required
          onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>) => {
            if (e.key === 'Enter') {
              send()
            }
          }}
        />
        {emailInvalid && (
          <small className="text-danger">
            Email invalid.
            <br />
          </small>
        )}
        {needEmail && (
          <small className="text-danger">Please input an email.</small>
        )}
      </>
    )
  }

  return (
    <DefaultModal
      show={show}
      title={'Do you hear that? It\'s your computer\'s fan thanking you.'}
      onClose={close}
      buttonText={'Submit'}
      action={send}
    >
      {meat}
    </DefaultModal>
  )
}

export default withSpeechContext(withMediaContext(DownloadEditedFileModal))
