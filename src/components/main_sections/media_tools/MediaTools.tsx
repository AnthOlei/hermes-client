import React from 'react'
import './media_tools.css'
import UploadFile from '../../helper_components/UploadFile'
import {
  withToastContext,
  withMediaContext,
  withSpeechContext,
  AddMediaContext,
  AddSpeechContext,
  AddToastContext,
} from '../../../contexts/HOCS'
import uploadFile from '../../../helpers/uploadFile'
import BlinkingEllipses from '../../helper_components/BlinkingEllipses'
import DownloadEditedFileModal from './helper_components/DownloadEditedFileModal'
import UploadedFile from '../../../data_classes/UploadedFile'

interface IMediaToolsProps
  extends AddMediaContext,
  AddSpeechContext,
  AddToastContext { }

class MediaTools extends React.Component<IMediaToolsProps> {
  state = {
    wasMediaPlayingBeforeSDown: false,
    sIsDown: false,
    showDownloadModal: false,
  }

  componentDidMount() {
    document.addEventListener('keydown', this.sDown, false)
    document.addEventListener('keyup', this.sUp, false)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.sDown, false)
    document.removeEventListener('keyup', this.sUp, false)
  }

  render() {
    const { mediaSrc } = this.props.mediaContext
    const { listening, floatingRequests } = this.props.speechContext
    const { showDownloadModal } = this.state

    return (
      <div className="toolbox main-section video-tools">
        <DownloadEditedFileModal
          show={showDownloadModal}
          onClose={() => this.setState({ showDownloadModal: false })}
        />
        <div className="toolbox__grid">
          <div className="mb-3">
            <div className="mx-3 mt-4 d-block">
              <UploadFile
                text={
                  mediaSrc ? 'Overwrite current file...' : 'Upload new file...'
                }
                classes=""
                onChange={this.userDidUploadFile}
              />
            </div>
            <div className="mx-3 mb-1 d-block"></div>
            {mediaSrc && (
              <div className="mx-3 mb-1 d-block">
                <button
                  type="button"
                  className="btn btn-light w-100 mb-2"
                  disabled={true}
                >
                  {`${floatingRequests} floating requests pending.`}
                </button>
                <button
                  type="button"
                  className="btn btn-light w-100"
                  disabled={!listening}
                >
                  {listening ? (
                    <BlinkingEllipses prefix="Listening" />
                  ) : (
                      'Hold "S" to take floating voice input...'
                    )}
                </button>
              </div>
            )}
            {mediaSrc && (
              <div className="mx-3 d-block">
                <button
                  type="button"
                  className="btn btn-primary w-100"
                  onClick={() => this.setState({ showDownloadModal: true })}
                >
                  Download Edited File
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

  async changeListeningState(value: boolean) {
    const { changeListeningState } = this.props.speechContext

    changeListeningState(value)
  }

  userDidUploadFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { toastContext, mediaContext } = this.props

    let file
    if (
      !event ||
      !event.target ||
      !event.target.files ||
      event.target.files.length < 1
    ) {
      toastContext.addToast('No file selected!', 'File Upload Error')
      return
    } else {
      file = event.target.files[0]
    }

    try {
      const preppedFile = uploadFile(file)
      mediaContext.uploadMedia(preppedFile)
    } catch (e) {
      toastContext.addToast(e.message, 'File Upload Error')
    }
  }

  sDown = (e: KeyboardEvent) => {
    const {
      mediaSrc,
      changePlayState,
      mediaIsPlaying,
    } = this.props.mediaContext
    const { listenForKeyboardEvents } = this.props.speechContext
    const { sIsDown } = this.state

    if (e.key === 's' && mediaSrc && !sIsDown && listenForKeyboardEvents) {
      let wasMediaPlaying
      if (mediaIsPlaying) {
        wasMediaPlaying = true
        changePlayState(false)
      } else {
        wasMediaPlaying = false
      }
      this.setState({
        wasMediaPlayingBeforeSDown: wasMediaPlaying,
        sIsDown: true,
      })
      changePlayState(false)
      this.changeListeningState(true)
    }
  }

  sUp = (e: KeyboardEvent) => {
    const { mediaSrc, changePlayState } = this.props.mediaContext
    const { wasMediaPlayingBeforeSDown } = this.state

    if (e.key === 's' && mediaSrc) {
      if (wasMediaPlayingBeforeSDown) {
        changePlayState(true)
      }
      this.setState({ sIsDown: false })
      this.changeListeningState(false)
    }
  }
}

export default withSpeechContext(withMediaContext(withToastContext(MediaTools)))
