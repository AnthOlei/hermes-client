import React from 'react'
import Toast from 'react-bootstrap/Toast'
import { ToastContext } from '../../contexts/toasts/ToastContext'

function Toaster(): JSX.Element {
  const toastContext = React.useContext(ToastContext)

  return (
    <div
      aria-live="polite"
      aria-atomic="true"
      style={{
        position: 'fixed',
        minHeight: '200px',
        minWidth: '400px',
      }}
    >
      {toastContext.toasts.map((toast, index) => (
        <div
          style={{
            position: 'fixed',
            top: 10 + index * 110 + 'px',
            right: '10px',
            zIndex: 10,
          }}
          key={index}
        >
          <Toast
            animation={true}
            onClose={() => toastContext.removeToast(toast.hash)}
          >
            <Toast.Header>
              <strong className="mr-auto">{toast.title}</strong>
            </Toast.Header>
            <Toast.Body className="bg-light text-dark">
              {toast.message}
            </Toast.Body>
          </Toast>
        </div>
      ))}
    </div>
  )
}

export default Toaster
