import React from 'react'

enum BreakPoints {
  small = 0,
  medium = 100,
  large = 200,
}

interface IResponsiveButtonProps {
  className: string
  width: number
  onClick: () => void
  text: [string, string] //medium, large
}

function ResponsiveButton(props: IResponsiveButtonProps): JSX.Element {
  const { className, width, onClick, text } = props

  let determinedText = ' '
  let classNameAdd = ' h-100'

  if (width > BreakPoints.large) {
    determinedText = text[1]
    classNameAdd = ''
  } else if (width > BreakPoints.medium) {
    determinedText = text[0]
    classNameAdd = ''
  }

  return (
    <button
      type="button"
      className={className + classNameAdd}
      onClick={onClick}
    >
      {determinedText}
    </button>
  )
}

export default ResponsiveButton
