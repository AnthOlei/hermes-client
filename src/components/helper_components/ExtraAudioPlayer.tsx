import React from 'react'
import { withMediaContext, AddMediaContext } from '../../contexts/HOCS'

interface IExtraAudioPlayerState {
  localTime: number
  mediaSrc: string | undefined
  mediaIsPlaying: boolean
  duration: number | undefined
}

class ExtraAudioPlayer extends React.Component<
  AddMediaContext,
  IExtraAudioPlayerState
> {
  private audioPlayerRef = React.createRef<HTMLAudioElement>()

  public state: IExtraAudioPlayerState = {
    localTime: 0,
    mediaSrc: undefined,
    mediaIsPlaying: false,
    duration: undefined,
  }

  componentDidUpdate() {
    this.determineSrc()
    this.determinePlay()
    this.determineNeedSkip()
    this.confirmVolume()
  }

  render() {
    const { mediaSrc } = this.state
    if (!mediaSrc) {
      return <div />
    }
    return (
      <audio
        src={mediaSrc}
        ref={this.audioPlayerRef}
        onLoadedMetadata={this.handleMetadata}
      />
    )
  }

  determineSrc = () => {
    const { mediaSrc } = this.state
    const { mediaContext } = this.props

    if (!mediaContext.addedMusic && mediaSrc) {
      this.setState({
        mediaSrc: undefined,
        duration: undefined,
        mediaIsPlaying: false,
      })
      return
    }
    if (!mediaContext.addedMusic) {
      return
    }
    const determinedSrc =
      process.env.PUBLIC_URL +
      `/audio/music/${mediaContext.addedMusic.musicType}.mp3`

    if (mediaSrc !== determinedSrc) {
      this.setState({
        mediaSrc: determinedSrc,
      })
    }
  }

  determinePlay = () => {
    const { mediaContext } = this.props
    const { mediaIsPlaying, mediaSrc } = this.state

    if (!mediaContext.addedMusic) {
      return
    }

    const currAudioPlayer = this.audioPlayerRef.current

    if (
      mediaSrc &&
      currAudioPlayer &&
      mediaContext.mediaIsPlaying !== mediaIsPlaying
    ) {
      mediaContext.mediaIsPlaying
        ? currAudioPlayer.play()
        : currAudioPlayer.pause()

      this.setState({ mediaIsPlaying: mediaContext.mediaIsPlaying })
    }
  }

  determineNeedSkip = () => {
    const { mediaContext } = this.props
    const { localTime, mediaSrc, duration } = this.state
    const THRESH_HOLD = 1

    const currAudioPlayer = this.audioPlayerRef.current

    if (
      !currAudioPlayer ||
      !mediaSrc ||
      !mediaContext.addedMusic ||
      !mediaContext.currentMediaTime
    ) {
      return
    }

    let secondsPlayerShouldBeAt =
      mediaContext.currentMediaTime - mediaContext.addedMusic.from

    if (duration) {
      secondsPlayerShouldBeAt = secondsPlayerShouldBeAt % duration
    }

    if (mediaContext.mediaIsTimelapsed) {
      secondsPlayerShouldBeAt /= 2
    }

    if (
      secondsPlayerShouldBeAt - THRESH_HOLD > localTime ||
      secondsPlayerShouldBeAt + THRESH_HOLD < localTime
    ) {
      currAudioPlayer.currentTime = secondsPlayerShouldBeAt
      this.setState({ localTime: secondsPlayerShouldBeAt })
    }
  }

  confirmVolume = () => {
    const { mediaSrc } = this.state
    const audioPlayerRef = this.audioPlayerRef.current
    if (mediaSrc && audioPlayerRef) {
      audioPlayerRef.volume = 0.05
    }
  }

  handleMetadata = (event: React.SyntheticEvent<HTMLAudioElement, Event>) => {
    this.setState({ duration: event.currentTarget.duration })
  }
}

export default withMediaContext(ExtraAudioPlayer)
