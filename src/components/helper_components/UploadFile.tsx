import React from 'react'

interface UploadFileProps {
  text: string
  classes: string
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

function UploadFile(props: UploadFileProps): JSX.Element {
  const fileUpload = React.useRef<HTMLInputElement>(null)

  return (
    <div className={'custom-file mx-auto d-table pt-3 ' + props.classes}>
      <input
        type="file"
        accept=".mov, .mp4, .avi"
        className="custom-file-input"
        id="customFile"
        onChange={props.onChange}
        ref={fileUpload}
        onClick={() => {
          const current = fileUpload.current
          if (current) {
            current.blur()
          }
        }}
      />
      <label className="custom-file-label" htmlFor="customFile">
        {props.text ?? 'Select a Video...'}
      </label>
    </div>
  )
}

export default UploadFile
