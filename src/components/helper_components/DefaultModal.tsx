import React from 'react'
import Modal from 'react-bootstrap/Modal'

interface IDefaultModalProps {
  action: () => void
  onClose: () => void
  title: string
  children: JSX.Element
  buttonText: string
  show: boolean
}

function DefaultModal(props: IDefaultModalProps): JSX.Element {
  const { action, onClose, title, children, buttonText, show } = {
    ...props,
  }

  return (
    <Modal show={show}>
      <Modal.Header closeButton onClick={onClose}>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
      <Modal.Footer>
        <button className="btn btn-secondary" onClick={onClose}>
          Close
        </button>
        <button className="btn btn-primary" onClick={action}>
          {buttonText}
        </button>
      </Modal.Footer>
    </Modal>
  )
}

export default DefaultModal
