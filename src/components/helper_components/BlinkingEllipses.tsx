import React, { useEffect, useState } from 'react'

interface IBlinkingEllipsesProps {
  prefix: string
}

function BlinkingEllipses(props: IBlinkingEllipsesProps): JSX.Element {
  let interval: NodeJS.Timer | undefined = undefined
  const [dotCount, setDotCount] = useState(1)

  const buildDots = (): string => {
    const { prefix } = props

    const dots = '.'
    return (prefix ?? '') + dots.repeat(dotCount)
  }

  const updateDots = (): void => {
    if (dotCount === 3) {
      setDotCount(1)
    } else {
      setDotCount(dotCount + 1)
    }
  }

  useEffect(() => {
    interval = setInterval(updateDots, 1000)

    return function cleanUpInterval() {
      if (interval) {
        clearInterval(interval)
      }
    }
  }, [])

  return <span>{buildDots()}</span>
}

export default BlinkingEllipses
