function secondsToHms(seconds: number | undefined): string {
  if (!seconds) {
    return '00:00:00'
  }
  return new Date(seconds * 1000).toISOString().substr(11, 8)
}

function HmsToMinutes(hms: string): string {
  if (!hms) return '0 minutes'
  const times = hms.split(':')
  if (parseInt(times[1]) < 1 && parseInt(times[0]) < 1) {
    return 'less than a minute'
  }
  return `about ${parseInt(times[0]) * 60 + parseInt(times[1])}`
}

export { secondsToHms, HmsToMinutes }
