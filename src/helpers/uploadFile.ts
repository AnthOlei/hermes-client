import UploadedFile from '../data_classes/UploadedFile'

function uploadFile(file: File): UploadedFile {
  return {
    url: URL.createObjectURL(file),
    file: file,
  }
}

export default uploadFile
