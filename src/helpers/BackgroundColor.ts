const dark = ['brown', 'black', 'blue', 'purple']
const light = ['white', 'red', 'green']

function backgroundColor(color: string): string {
  if (light.includes(color)) {
    return '#000000f0'
  }
  return '#fffffff0'
}

export default backgroundColor
